package uz.mamarasulov.gmailauthdemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by WindowsAnniversary on 6/20/2017.
 */

public class MyUtils {
    public static void putSharedPrefString(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static void putSharedPrefBoolean(Context context, String key, boolean state) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, state);
        editor.commit();
    }

    public static String getSharedPrefString(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }
    public static boolean    getSharedPrefBoolean(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, false);
    }
/*
    public static void printErrorResponse(InputStream errorStream, String urlStr, String TAG) {
        try {
            BufferedReader responseStream = new BufferedReader(new InputStreamReader(errorStream, Constants.UTF_8));
            String line;
            StringBuilder errorResponse = new StringBuilder();
            while ((line = responseStream.readLine()) != null) {
                errorResponse.append(line);
            }
            StringBuilder errorMgs = new StringBuilder();
            errorMgs.append("Error received while requesting: ");
            errorMgs.append(urlStr);
            errorMgs.append("Error: ");
            errorMgs.append(errorResponse.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
*/
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
