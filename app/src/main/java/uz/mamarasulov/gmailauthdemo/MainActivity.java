package uz.mamarasulov.gmailauthdemo;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;


public class MainActivity extends AppCompatActivity {

    private static final String EMAIL = "email";
    private static final String PUBLIC_PROFILE = "public_profile";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_FRIENDS = "user_friends";
    private static final String USER_PHOTOS = "user_photos";
    private static final String USER_BIRTHDAY = "user_birthday";


    private final int RC_SIGN_IN = 420;
    private ImageView ivGoogleSignButton;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager facebookCallbackManager;
    private ProfileTracker profileTracker;
    private String userFullName, userTokenGoogle, userEmail, authTokenFacebook;
    private TextView tvResult;
    private Button facebookLogoutButton;
    private ImageView facebookUserPhoto;
    private boolean loggedOut;
    private AccessToken facebookAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.setApplicationId("159254431424595");
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_main);


        tvResult = findViewById(R.id.tvResult);
        facebookLogoutButton = findViewById(R.id.facebook_sign_out);
        facebookUserPhoto = findViewById(R.id.facebook_user_photo);

        googleInit();

        facebookInit();
    }


    private void googleInit() {
        ivGoogleSignButton = findViewById(R.id.google_sign_in);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        ivGoogleSignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
    }

    private void facebookInit() {
        ImageView ivFacebookSignButton = findViewById(R.id.facebook_sign_in);
        loggedOut = AccessToken.getCurrentAccessToken() == null;

        facebookCallbackManager = CallbackManager.Factory.create();

        ivFacebookSignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookSignIn();
            }
        });

        facebookLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                faceBookSignOut();
            }
        });
    }

    private void faceBookSignOut() {
        profileTracker.stopTracking();
        LoginManager.getInstance().logOut();
        Toast.makeText(MainActivity.this, "Logged out", Toast.LENGTH_SHORT).show();
        facebookLogoutButton.setVisibility(View.GONE);
        tvResult.setVisibility(View.GONE);
        facebookUserPhoto.setVisibility(View.GONE);
    }

    private void facebookSignIn() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(
                            Profile oldProfile,
                            Profile currentProfile) {
                        if (AccessToken.getCurrentAccessToken() != null)
                            facebookShowDialog(currentProfile);
                        else
                            Toast.makeText(MainActivity.this, "already logged in", Toast.LENGTH_SHORT).show();
                    }
                };
                facebookAccessToken = AccessToken.getCurrentAccessToken();
            }


            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void googleSignOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(MainActivity.this, "Signed out", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount account) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else if (facebookCallbackManager != null)
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            updateUI(account);
            googleShowDialog();
        } catch (ApiException e) {
            Toast.makeText(this, "signInResult: failed code=" + e.getStatusCode(), Toast.LENGTH_SHORT).show();
            updateUI(null);
        }
    }

    private void googleShowDialog() {

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(MainActivity.this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            String personToken = acct.getIdToken();
            String personPhoto = "";
            if (acct.getPhotoUrl() != null)
                personPhoto = acct.getPhotoUrl().toString();

            ImageView userImage = new ImageView(MainActivity.this);


            Glide.with(MainActivity.this)
                    .load(personPhoto)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_wallpaper_black_24dp))
                    .into(userImage);


            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setTitle("User Information");
            alertDialog.setView(userImage);
            alertDialog.setMessage(
                    "\nGiven name: " + personGivenName
                            + "\nFamily name: " + personFamilyName
                            + "\nEmail: " + personEmail
                            + "\nID: " + personId
                    //+ "\nIDToken: " + personToken
            );
            alertDialog.setPositiveButton("OK", null);
            alertDialog.setNegativeButton("Sign out", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    googleSignOut();
                }
            });

            alertDialog.show();
        }
    }

    private void facebookShowDialog(Profile profile) {
        tvResult.setVisibility(View.VISIBLE);
        tvResult.setText("User : " + profile.getName() + "\n"
                + "Id : " + profile.getId() + "\n"
                + "Access Token : " + facebookAccessToken.getToken().toString());

        facebookUserPhoto.setVisibility(View.VISIBLE);
        Glide.with(MainActivity.this)
                .load(profile.getProfilePictureUri(120, 120))
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_wallpaper_black_24dp))
                .into(facebookUserPhoto);

        facebookLogoutButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (profileTracker != null)
            profileTracker.stopTracking();
    }
}
